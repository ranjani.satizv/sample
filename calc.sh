# !/usr/bin/bash
 
 
# Input type of operation
echo "Enter Choice :"
echo "1. Addition"
echo "2. Subtraction"
echo "3. Multiplication"
echo "4. Division"
read choice
# Read input from the user
case "$choice" in
1)
	read -p "Enter first number:" a
	read -p "Enter second number:" b
	res=`expr $a + $b`
	echo $a + $b is $res

;;
2)
	read -p "Enter first number:" a
	read -p "Enter second number:" b
	res=`expr $a - $b`
	echo $a - $b is $res
;;
3)
	read -p "Enter first number:" a
	read -p "Enter second number:" b
	res=`expr $a \* $b`
	echo $a "*" $b is $res

;;
4)
	read -p "Enter first number:" a
	read -p "Enter second number:" b
	res=`expr $a / $b`
	echo $a / $b is $res
;;
*)
	echo "Enter Valid Operation"
;;
esac
