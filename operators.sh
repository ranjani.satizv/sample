#/usr/bin/bash

# Author: Ranjani Subramanian
# This script is to understamd the use cases of different operators

echo "What Shell Operator do you want to test?"
echo "1. Arithmetic"
echo "2. Relational"
echo "3. Boolean"
echo "4. String"
echo "5. File Test"
read choice
# Read input from the user
case "$choice" in
1)
	read -p "Enter a value for a:" a 
	read -p "Enter a value for b:" b 
	echo "The numbers entered are $a and $b"
	add=`expr $a + $b`
	echo $a + $b is $add
	mod=`expr $a % $b`
	echo "$a "%" $b is $mod"
;;
2)

	read -p "Enter a value for a:" a 
	read -p "Enter a value for b:" b 
	echo "The numbers entered are $a and $b"
	if [ $a -eq $b ] 
	then
  		echo $a "=" $b 
	else
  		echo $a and $b are not equal
	fi
	if [ $a -gt $b ]
	then
  		echo $a is greater than $b
	else
  		echo $a is not greater than $b
	fi
;;
3)
	read -p "Enter a value for a:" a 
	read -p "Enter a value for b:" b 
	echo "The numbers entered are $a and $b"
	if [ $a -lt 10 -a $b -gt 10 ]
	then
  		echo $a and $b are greater than 10
	else
  		echo "$a and $b are not greater than 10"
	fi
	if [ $a -lt 100 -o $b -lt 100 ]
	then
  		echo Either $a or $b is greater than 10
	else
  		echo Both $a and $b  are smaller than 10
	fi
;;
4)
	read -p "Enter a string:" c
	if [ -z $c ]
	then
   		echo "-z $c : string length is zero"
	else
   		echo "-z $c : string length is not zero"
	fi

	if [ $c ]
	then
   		echo " $c : string is not empty"
	else
   		echo "-n $c : string is empty"
	fi
;;
5)
	file="/home/ec2-user/hello.sh"
	if [ -r $file ]
	then
   		echo "File has read access"
	else
   		echo "File does not have read access"
	fi
	if [ -w $file ]
	then
   		echo "File has write permission"
	else
   		echo "File does not have write permission"
	fi
;;
*)
	echo "Enter Valid Operation"
;;
esac





